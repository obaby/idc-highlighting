# IDC Script language specification for Crimson Editor v3.70
#
# Edit history:
# Initial release: 2005-06-09 Sebastian Porst

$CASESENSITIVE=YES
$DELIMITERS=~`!@#$%^&*()-+=|\{}[]:;"',.<>/?
$KEYWORDPREFIX=#
$HEXADECIMALMARK=0x
$ESCAPECHAR=\
$QUOTATIONMARK1="
$QUOTATIONMARK2='
$LINECOMMENT=//
$BLOCKCOMMENTON=/*
$BLOCKCOMMENTOFF=*/
$INDENTATIONON={
$INDENTATIONOFF=}
$PAIRS1=()
$PAIRS2={}
$PAIRS3=[]
