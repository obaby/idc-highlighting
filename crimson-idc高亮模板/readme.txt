Crimson Editor Syntax definition files for the IDC scripting language
of the Interactive Disassembler IDA (http://www.datarescue.com)

extension.idc - Goes into Crimson Editor\link
idc.key - Goes into Crimson Editor\spec
idc.spc - Goes into Crimson Editor\spec
example.idc - Example file to test syntax highlighting
parse.pl - Used to parse the IDC.IDC file toproduce the biggest part of idc.key.

Copyright 2005 by Sebastian Porst (webmaster@the-interweb.com)