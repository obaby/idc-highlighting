// Example file to demonstrate syntax highlighting for IDC scripts
// in Crimson Editor.

/*
* Edit history:
* 
* Initial release: 2005-06-09 Sebastian Porst
*/

#include <idc.idc>

static main()
{
	auto x;
	
	for (x=0;x<10;x++)
	{
		Message("Test " + ltoa(x, 10) + "\n");
		
		if (x == BADADDR)
		{
			Message("This message is never shown.");
		}
	}
}