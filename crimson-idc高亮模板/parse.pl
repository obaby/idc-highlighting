# Perl script to create Crimson Editor custom syntax files for IDC scripts
# 
# This script parses the file IDC.IDC which contains the definitions of all
# standard functions and macros that can be used in IDC scripts. All recognized
# keywords and functions are printed in sorted order to stdout.
#
# Edit history:
# Initial release: 2005-06-09 Sebastian Porst
#
# This script is licensed under the zlib/libpng license.

use strict;

open(FILE, "idc.idc");

my @keywords;
my @functions;

while(<FILE>)
{
	my $line = $_;
	
	if ($line =~ /\#define\s*(\w+)\s/)
	{
		# Keywords of syntax #define FOO ...
		push(@keywords, $1);
	}
	elsif ($line = /\#define\s*(\w+)/)
	{
		# Functions of syntax #define Foo(X) ...
		@functions = (@functions, $1);
	}
	elsif ($line = /^\w+\s+or\s+\w+\s+(\w+)/)
	{
		# Functions of syntax TYPE or TYPE Foo()
		@functions = (@functions, $1);
	}
	elsif ($line = /^\w+\s+(\w+)\s*\(/)
	{
		# Functions of syntax TYPE Foo()
		@functions = (@functions, $1);
	}
}

# Sort and remove duplicates

@keywords = sort(@keywords);
@functions = sort(@functions);

my $prev = '';

@keywords = grep($prev ne $_ && ($prev = $_), @keywords);
@functions = grep($prev ne $_ && ($prev = $_), @functions);

# Print results

print "[KEYWORDS2:GLOBAL]\n";

foreach my $kw (@keywords)
{
	print $kw. "\n";
}

print "\n[KEYWORDS0:GLOBAL]\n";

foreach my $f (@functions)
{
	print $f. "\n";
}
